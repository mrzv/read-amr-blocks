#include <diy/master.hpp>
#include <diy/assigner.hpp>
#include <diy/link.hpp>
#include <diy/fmt/format.h>
#include <diy/io/block.hpp>         // for saving blocks in DIY format

#include "fab-block.h"
using Real  = double;
using Block = FabBlock<Real, 3>;

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        fmt::print(std::cerr, "Usage: {} IN.amr\n", argv[0]);
        return 1;
    }
    std::string infn = argv[1];

    diy::mpi::environment   env(argc, argv);
    diy::mpi::communicator  world;
    diy::Master             master(world,
                                   1, -1,
                                   Block::create,
                                   Block::destroy,
                                   0,
                                   Block::save,
                                   Block::load);

    diy::ContiguousAssigner assigner(world.size(), 0);
    diy::MemoryBuffer       header;

    diy::io::read_blocks(infn, world, assigner, master, header);

    diy::DiscreteBounds domain;
    diy::load(header, domain);
    fmt::print("Domain: {} - {}\n", domain.min, domain.max);

    master.foreach([](Block* b, const diy::Master::ProxyWithLink& cp)
                   {
                     auto* l = static_cast<diy::AMRLink*>(cp.link());
                     fmt::print("{}: level = {}, refinement = {}, shape = {}, core = {} - {}, bounds = {} - {}\n",
                                cp.gid(), l->level(), l->refinement(), b->fab.shape(),
                                l->core().min, l->core().max,
                                l->bounds().min, l->bounds().max,
                                *(b->fab.data() + 1));

                     for(int j = 0; j < l->size(); ++j) {
                         fmt::print("--link member {}: level = {}, refinement = {}, shape = {}, core = {} - {}, bounds = {} - {}\n",
                                    j, l->level(j), l->refinement(j), b->fab.shape(),
                                    l->core(j).min, l->core(j).max,
                                    l->bounds(j).min, l->bounds(j).max);


                     }
                   });
    // cp,enqueue(neighbour, i, j)
    // master.exchange()
    // sample.cpp
}
